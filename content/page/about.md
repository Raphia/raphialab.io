---
title: About me
subtitle: A simple guy interested in many things
date: 2020-03-08T08:47:11+01:00
comments: false
---

My name is Raphael and here
I want to collect some of my thoughts
on Math, Physics, Computer Science, Languages and Life in general.  
It's first and foremost for myself, but you are 
very welcomed to read my posts and discuss with me.
