+++
title = "My very first post."
date = 2020-03-08T08:47:11+01:00
comments = true
+++

I was thinking about keeping a little blog for
quite a long time and today is finally the day I started one.   

Really hope that I find the time and motivation to fill
it with some interesting content. And even though it is mainly supposed to
be some sort of public notebook or diary for myself, I'd 
be amazed if I can get some interesting feedback of you.



